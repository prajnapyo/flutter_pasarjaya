import 'package:get/get.dart';

class NavigationController extends GetxController {
  var currentIndex = 0;

  void onTabTapped(int index) {
    print(index);
    currentIndex = index;
  }
}