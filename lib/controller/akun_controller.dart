import 'package:flutter_learn/model/akun.dart';
import 'package:flutter_learn/service/akun_service.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class AkunController extends GetxController {
  static const _pageSize = 10;
  final listAkun = <Akun>[].obs;
  final PagingController<int, Akun> _pagingController =
      PagingController(firstPageKey: 1);

  PagingController<int, Akun> get getPagingController => _pagingController;
  List<Akun> get getListAkun => listAkun;

  @override
  void onInit() async {
    _pagingController.addPageRequestListener((pageKey) {});

    fetchAkun();
    super.onInit();
  }

  void refresh() async {
    await _fetchPage(1);
    fetchAkun();
  }

  void fetchAkun() async {
    await AkunService().getAkun().then((value) {
      listAkun(value);
    }).catchError((e) {
      print('err');
    });
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final dataItems = await AkunService().getAkun();
      final isLastPage = dataItems.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage(dataItems);
      } else {
        final nextPageKey = pageKey + dataItems.length;
        _pagingController.appendPage(dataItems, nextPageKey);
      }
    } catch (e) {
      _pagingController.error = e;
    }
  }
}
