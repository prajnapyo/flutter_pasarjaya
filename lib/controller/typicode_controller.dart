import 'package:flutter_learn/model/typicode.dart';
import 'package:flutter_learn/service/api_service.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class TypiCodeController extends GetxController {
  final service = Get.find<ApiService>();
  final listTypiCode = <TypiCode>[].obs;
  final PagingController<int, TypiCode> _pagingController =
      PagingController(firstPageKey: 1);
  static const _pageSize = 5;

  PagingController<int, TypiCode> get getPagingController => _pagingController;
  List<TypiCode> get getListTypiCode => listTypiCode;

  @override
  void onInit() async {
    _pagingController.addPageRequestListener((pageKey) async {
      await _fetchPage(pageKey);
    });

    fetchTypiCode();
    super.onInit();
  }

  void refresh() async {
    await _fetchPage(1);
    fetchTypiCode();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final dataItems = await service.getTypiCode();
      print(dataItems.length);
      final isLastPage = dataItems.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage(dataItems);
      } else {
        final nextPageKey = pageKey + dataItems.length;
        _pagingController.appendPage(dataItems, nextPageKey);
      }
    } catch (e) {
      _pagingController.error = e;
    }
  }

  void fetchTypiCode() async {
    await service.getTypiCode().then((value) {
      listTypiCode(value);
    }).catchError((e) {
      print('err');
    });
  }
}
