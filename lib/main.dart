import 'package:flutter/material.dart';
import 'package:flutter_learn/controller/akun_controller.dart';
import 'package:flutter_learn/controller/navigation_controller.dart';
import 'package:flutter_learn/controller/typicode_controller.dart';
import 'package:flutter_learn/controller/zoom_drawer_controller.dart';
import 'package:flutter_learn/form_validate.dart';
import 'package:flutter_learn/service/api_service.dart';
import 'package:flutter_learn/store/setup.dart';
import 'package:flutter_learn/widgets/home_page.dart';
import 'package:get/get.dart';
// import 'package:tutorial_flutter/hero.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setup();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      home: FormValidate(),
      initialBinding: BindingsBuilder(() {
        Get.lazyPut(() => NavigationController(), fenix: true);
        Get.lazyPut(() => MyZoomDrawer(), fenix: true);
        Get.lazyPut(() => AkunController(), fenix: true);
        Get.lazyPut(() => TypiCodeController(), fenix: true);
        Get.lazyPut(() => ApiService(), fenix: true);
      }),
    );
  }
}
