import 'package:flutter/material.dart';
import 'package:flutter_learn/widgets/loading.dart';
import 'package:flutter_learn/widgets/todo_list_view.dart';
import '../store/todo_store.dart';
import '../widgets/widgets.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final TodoStore store = TodoStore();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('MobX Todos List'),
        elevation: 0,
      ),
      body: Stack(
        children: [
          Loading(store: store),
          TodoListView(store: store),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => store.fetchTodos(),
        child: const Icon(
          Icons.refresh,
        ),
      ),
    );
  }
}
