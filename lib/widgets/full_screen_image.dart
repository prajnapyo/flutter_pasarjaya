import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class FullScreenImage extends StatefulWidget {
  FullScreenImage({this.initialIndex = 0})
      : controller = PageController
    (initialPage: initialIndex);

  final int initialIndex;
  final PageController controller;

  @override
  _FullScreenImage createState() => _FullScreenImage();
}

class _FullScreenImage extends State<FullScreenImage> {
  List<String> image = [
    'https://i.pinimg.com/originals/eb/ac/4a/ebac4ab84063d2b52b3503b2575e3b41.jpg',
    'https://i.pinimg.com/originals/27/5e/31/275e31bc67ec28b5882e2ee333701fe1.jpg'
  ];
  late int currentIndex = widget.initialIndex;

  void onPageChanged(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            child: PhotoViewGallery.builder(
              loadingBuilder: (context, event) => Center(
                child: Container(
                  color: Colors.black,
                  child: Center(
                    child: CircularProgressIndicator(
                      strokeWidth: 2.0,
                    ),
                  ),
                ),
              ),
              itemCount: 4,
              backgroundDecoration: BoxDecoration(color: Colors.black),
              pageController: widget.controller,
              onPageChanged: onPageChanged,
              builder: (context, index) {
                return PhotoViewGalleryPageOptions(
                  imageProvider: CachedNetworkImageProvider('${image[index]}'),
                  initialScale: PhotoViewComputedScale.contained * 0.8,

                );
              },
            ),
          )
        ],
      ),
    );
  }
}
