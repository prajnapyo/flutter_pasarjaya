import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_learn/constants.dart';
import 'package:flutter_learn/model/typicode.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:flutter_learn/service/secure_storage.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:http/http.dart' as http;

class ApiService extends GetxService{
  Dio get dio => _dio();

  Dio _dio() {
    final options = BaseOptions(
      connectTimeout: connectTimeout,
      receiveTimeout: receiveTimeout,
      contentType: "application/json;charset=utf-8",
    );
    var dio = Dio(options);

    if (foundation.kDebugMode) {
      dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: true,
        compact: true,
      ));
    } else {}

    return dio;
  }

  final storage = const FlutterSecureStorage();

  final SecureStorage _secureStorage = SecureStorage();

  Future<List<TypiCode>> getTypiCode() async {
    try {
      var response = await dio.get('${ApiConstants.baseUrl}/posts');
      if (response.statusCode == 200) {
        List model = response.data;
        return model.map((f) => TypiCode.fromJson(f)).toList();
      } else {
        return [];
      }
    } catch (e) {
      throw Exception('Failed to load');
    }
  }
  
  Future cobaApi(String name, String password) async {
    var response = await dio.post('http://172.17.2.107/pasarjaya/admin/api/login/login', data: {
      'username': name, 'password': password, 'device': 123456, 'device_name': 'Xiaomi'
    }, options: Options(contentType: Headers.formUrlEncodedContentType));
    return response.data;
  }

  Future login(String email, String password) async {
    var response = await dio.post('http://172.17.1.207:3222/auth/login', data: {
      'email': email, 'password' : password
    });
    await _secureStorage.setToken(response.data['data']['token']);
    await _secureStorage.setUserName(response.data['message']);
    return response.data['data'];
  }
}
