import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter_learn/constants.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:flutter_learn/model/akun.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:http/http.dart' as http;

class AkunService {
  // Dio get dio => _dio();
  //
  Dio _dio() {
    final options = BaseOptions(
      connectTimeout: connectTimeout,
      receiveTimeout: receiveTimeout,
      contentType: "application/json;charset=utf-8",
    );
    var dio = Dio(options);

    if (foundation.kDebugMode) {
      dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: true,
        compact: true
      ));
    } else {}

    return dio;
  }


  Future<List<Akun>> getAkun() async {
    try {
      var response = await Dio().get('http://172.17.2.107/pasarjaya/admin/akun/data/');
      if (response.statusCode == 200) {
        final List rawData = response.data['data'];
        return rawData.map((f) => Akun.fromJson(f)).toList();
      } else {
        // print('${response.statusCode} : ${response.data.toString()}');
        throw Exception('Failed to load');
      }
    } catch(e) {
      throw Exception('Failed to load');
    }
  }

  Future<List<Akun>> searchAkun(String name) async {
    try {
      var response = await _dio().get('http://172.17.2.107/pasarjaya/admin/akun/data?name=$name');
      if (response.statusCode == 200) {
        final List rawData = response.data['data'];
        return rawData.map((f) => Akun.fromJson(f)).toList();
      } else {
        // print('${response.statusCode} : ${response.data.toString()}');
        throw Exception('Failed to load');
      }
    } catch(e) {
      throw Exception('Failed to load');
    }
  }

  Future<List<Akun>> getDetailAkun(String id) async {
    print(id);
    var response = await Dio().get('http://172.17.2.107/pasarjaya/admin/akun/data/detail?id=$id');
    if(response.statusCode == 200 ){
      List result = response.data['data'];
      return result.map((f) => Akun.fromJson(f)).toList();
    } else {
      return throw Exception('data tidak ditemukan');
    }
}

  Future<int> createAkun(String name, int age, String alamat) async {
      var response = await Dio().post('http://172.17.2.107/pasarjaya/admin/akun/data/create',  data: {
        'name': name,
        'age': age.toString(),
        'alamat': alamat
      }, options: Options(
        contentType: Headers.formUrlEncodedContentType,
      ),);
      return response.statusCode == 200 ? 1 : 0;
  }

  Future<int> updateAkun(String id, String name, int age, String alamat) async {
    print(age.toString());
    print(alamat);
    var response = await Dio().put('http://172.17.2.107/pasarjaya/admin/akun/data/update?id=$id', data: {
      'name': name,
      'age': age,
      'alamat': alamat
    },options: Options(
      contentType: Headers.formUrlEncodedContentType,
    ));
    return response.statusCode == 200 ? 1 : 0;
  }

  Future<int> deleteAkun(String id) async {
    var response = await Dio().delete('http://172.17.2.107/pasarjaya/admin/akun/data/delete?id=$id');
        return response.statusCode == 200 ? 1 : 0;
  }
}
