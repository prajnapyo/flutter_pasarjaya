import 'package:badges/badges.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_learn/controller/navigation_controller.dart';
import 'package:flutter_learn/controller/zoom_drawer_controller.dart';
import 'package:flutter_learn/home.dart';
import 'package:flutter_learn/ui/bottom_navigation.dart';
import 'package:flutter_learn/ui/home.dart';
import 'package:flutter_learn/ui/main_screen.dart';
import 'package:flutter_learn/ui/menu_screen.dart';
import 'package:flutter_learn/ui/task_screen.dart';
import 'package:flutter_learn/ui/zoom_drawer_screen.dart';
import 'package:flutter_learn/widgets/full_screen_image.dart';
import 'package:flutter_zoom_drawer/config.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeScreenPage extends StatefulWidget {
  const HomeScreenPage({Key? key}) : super(key: key);

  @override
  _HomeScreenPage createState() => _HomeScreenPage();
}

class _HomeScreenPage extends State<HomeScreenPage>
    with TickerProviderStateMixin {
  final controller = Get.find<NavigationController>();
  final controller1 = Get.find<MyZoomDrawer>();
  late AnimationController animationController;

  @override
  initState() {
    super.initState();
    animationController = BottomSheet.createAnimationController(this);
    // Animation duration for displaying the BottomSheet
    animationController.duration = const Duration(seconds: 1);
    // Animation duration for retracting the BottomSheet
    animationController.reverseDuration = const Duration(seconds: 1);
    animationController.drive(CurveTween(curve: Curves.easeIn));
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(60),
          child: AppBar(
            backgroundColor: Colors.brown[50],
            elevation: 0,
            leading: InkWell(
              onTap: () {
                controller1.toggleDrawer;
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return ZoomDrawerScreen();
                },));
              },
              splashColor: null,
              highlightColor: null,
              child: Container(
                margin: const EdgeInsets.only(left: 20),
                width: 40,
                height: 40,
                decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: NetworkImage(
                            'https://i.pinimg.com/originals/89/ba/ea/89baead9a793409062027fc707f5874a.jpg'),
                        fit: BoxFit.fill)),
              ),
            ),
            title: const Text(
              'Hi, Kira!',
              style: TextStyle(color: Colors.black54),
            ),
            actions: [
              Container(
                  padding: const EdgeInsets.only(right: 20),
                  child: Badge(
                    alignment: Alignment.center,
                    position:     BadgePosition.topEnd(top: 12, end: 4),
                    child: Icon(
                      Icons.notifications,
                      color: Colors.black54,
                      size: 28,
                    ),
                  )),
              // Badge(
              //   badgeColor: Colors.red,
              //   child: Icon(Icons.notifications),
              // ),
              // Stack(
              //   alignment: Alignment.center,
              //   children: [
              //     Container(
              //       padding: const EdgeInsets.only(right: 20),
              //       child: const Icon(
              //         Icons.notifications,
              //         color: Colors.black54,
              //       ),
              //     ),
              //     Positioned(
              //         top: 19,
              //         left: 14,
              //         child: Container(
              //           width: 6,
              //           height: 6,
              //           decoration: const BoxDecoration(
              //               shape: BoxShape.circle, color: Colors.redAccent),
              //         )),
              //   ],
              // )
            ],
          ),
        ),
        body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.brown[50],
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Tasks for today:',
                        style: TextStyle(
                            fontSize: 30, fontWeight: FontWeight.w700),
                      ),
                      Row(
                        children: [
                          const Icon(
                            Icons.star,
                            color: Colors.orange,
                          ),
                          const Text(
                            '5 available',
                            style: TextStyle(fontSize: 18),
                          )
                        ],
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.02,
                      ),
                      TextField(
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            hintText: 'Search',
                            contentPadding: const EdgeInsets.all(25),
                            hintStyle: TextStyle(color: Colors.brown[100]),
                            suffixIcon: Padding(
                                padding: const EdgeInsets.only(right: 20),
                                child: Icon(
                                  Icons.search,
                                  color: Colors.brown[200],
                                  size: 25,
                                )),
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                    width: 0, color: Colors.white)),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                    width: 0, color: Colors.white))),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.04,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            'Last connections',
                            style: TextStyle(
                                fontWeight: FontWeight.w700, fontSize: 20),
                          ),
                          InkWell(
                              onTap: () {
                                Navigator.push(context, MaterialPageRoute(
                                  builder: (context) {
                                    return FullScreenImage();
                                  },
                                ));
                              },
                              child: Text(
                                'See all',
                                style: TextStyle(
                                    color: Colors.brown[200],
                                    fontWeight: FontWeight.w500),
                              ))
                        ],
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.01,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: 55,
                            height: 55,
                            decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: NetworkImage(
                                        'https://i.pinimg.com/originals/89/ba/ea/89baead9a793409062027fc707f5874a.jpg'),
                                    fit: BoxFit.fill)),
                          ),
                          Container(
                            width: 55,
                            height: 55,
                            decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: NetworkImage(
                                        'https://i.pinimg.com/originals/89/ba/ea/89baead9a793409062027fc707f5874a.jpg'),
                                    fit: BoxFit.fill)),
                          ),
                          Container(
                            width: 55,
                            height: 55,
                            decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: NetworkImage(
                                        'https://i.pinimg.com/originals/89/ba/ea/89baead9a793409062027fc707f5874a.jpg'),
                                    fit: BoxFit.fill)),
                          ),
                          Container(
                            width: 55,
                            height: 55,
                            decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: NetworkImage(
                                        'https://i.pinimg.com/originals/89/ba/ea/89baead9a793409062027fc707f5874a.jpg'),
                                    fit: BoxFit.fill)),
                          ),
                          Container(
                            width: 55,
                            height: 55,
                            decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                            ),
                            child: FloatingActionButton(
                              backgroundColor: Colors.white54,
                              onPressed: () async {
                                String telephoneNumber = '+234214124213';
                                Uri telephoneUrl =
                                    Uri.parse('tel:$telephoneNumber');
                                if (await canLaunchUrl(telephoneUrl)) {
                                  await launchUrl(telephoneUrl);
                                } else {
                                  throw "error";
                                }
                              },
                              child: const Text(
                                '+5',
                                style: TextStyle(color: Colors.black54),
                              ),
                              elevation: 0,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.03,
                ),
                Expanded(
                    child: Container(
                  height: MediaQuery.of(context).size.height,
                  decoration: const BoxDecoration(
                      color: Colors.white54,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(25),
                          topLeft: Radius.circular(25))),
                  padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          const Text(
                            'Last connections',
                            style: TextStyle(
                                fontWeight: FontWeight.w700, fontSize: 20),
                          ),
                          InkWell(
                              onTap: () {
                                controller.onTabTapped(2);
                                showModalBottomSheet(
                                  isScrollControlled: true,
                                  context: context,
                                  transitionAnimationController:
                                      animationController,
                                  builder: (context) {
                                    return Scaffold(
                                      appBar: PreferredSize(
                                          preferredSize: Size.fromHeight(100),
                                          child: AppBar(
                                            elevation: 0,
                                            automaticallyImplyLeading: false,
                                            backgroundColor: Color(0xffd1edbf),
                                            flexibleSpace: Container(
                                              alignment: Alignment.center,
                                              padding: EdgeInsets.only(
                                                  left: 20, right: 20),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Container(
                                                    width: 50,
                                                    height: 50,
                                                    decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        border: Border.all(
                                                            color: Colors.grey,
                                                            width: 1,
                                                            style: BorderStyle
                                                                .solid)),
                                                    child: FloatingActionButton(
                                                      elevation: 0,
                                                      focusColor:
                                                          Colors.white54,
                                                      backgroundColor:
                                                          Color(0xffd1edbf),
                                                      onPressed: () {
                                                        Navigator.pop(context);
                                                      },
                                                      child: const Icon(
                                                        Icons.close,
                                                        color: Colors.black,
                                                        size: 30,
                                                      ),
                                                    ),
                                                  ),
                                                  IconButton(
                                                      onPressed: () {
                                                        Navigator.push(context,
                                                            MaterialPageRoute(
                                                          builder: (context) {
                                                            return Home();
                                                          },
                                                        ));
                                                      },
                                                      icon: Icon(
                                                        Icons.more_horiz,
                                                        size: 30,
                                                      ))
                                                ],
                                              ),
                                            ),
                                          )),
                                      body: Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        height:
                                            MediaQuery.of(context).size.height,
                                        color: Color(0xffd1edbf),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Expanded(
                                                child: Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              padding: EdgeInsets.only(
                                                  left: 30, right: 30, top: 60),
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.only(
                                                          topLeft:
                                                              Radius.circular(
                                                                  50),
                                                          topRight:
                                                              Radius.circular(
                                                                  50))),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    'Messaging ID',
                                                    style: TextStyle(
                                                        fontSize: 30,
                                                        fontWeight:
                                                            FontWeight.w900),
                                                  ),
                                                  SizedBox(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            0.03,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text(
                                                        'Your daily plan',
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.w800,
                                                            fontSize: 17),
                                                      ),
                                                      Text('70%',
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w800,
                                                              fontSize: 17))
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            0.015,
                                                  ),
                                                  LinearPercentIndicator(
                                                    percent: 0.7,
                                                    barRadius:
                                                        Radius.circular(20),
                                                    progressColor: Colors.black,
                                                    lineHeight: 10,
                                                  ),
                                                  SizedBox(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            0.015,
                                                  ),
                                                  Text(
                                                    '4 of 6 completed',
                                                    style: TextStyle(
                                                        color: Colors.grey,
                                                        fontWeight:
                                                            FontWeight.w600),
                                                  ),
                                                  SizedBox(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            0.03,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Card(
                                                        color:
                                                            Color(0xffffe7ab),
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10)),
                                                        elevation: 0,
                                                        child: Container(
                                                          width: 165,
                                                          padding:
                                                              EdgeInsets.all(
                                                                  15),
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Text(
                                                                '17',
                                                                style: TextStyle(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600,
                                                                    fontSize:
                                                                        30),
                                                              ),
                                                              Row(
                                                                children: [
                                                                  Icon(Icons
                                                                      .task),
                                                                  Text(
                                                                    ' Task finished',
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .black54),
                                                                  )
                                                                ],
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                      Card(
                                                        color:
                                                            Color(0xffffe7ab),
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10)),
                                                        elevation: 0,
                                                        child: Container(
                                                          width: 165,
                                                          padding:
                                                              EdgeInsets.all(
                                                                  15),
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Text(
                                                                '3,2',
                                                                style: TextStyle(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600,
                                                                    fontSize:
                                                                        30),
                                                              ),
                                                              Row(
                                                                children: [
                                                                  Icon(Icons
                                                                      .lock_clock),
                                                                  Text(
                                                                      ' Tracked hours',
                                                                      style: TextStyle(
                                                                          color:
                                                                              Colors.black54))
                                                                ],
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            0.03,
                                                  ),
                                                  Text(
                                                    'Overview',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w800,
                                                        fontSize: 17),
                                                  ),
                                                  SizedBox(
                                                      height:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .height *
                                                              0.02),
                                                  Text(
                                                    'Messaging ID framework development for marketing branch and the publicity bureau and implemented draft on the framework',
                                                    style: TextStyle(
                                                        color: Colors.black54),
                                                  ),
                                                  SizedBox(
                                                      height:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .height *
                                                              0.05),
                                                  Text(
                                                    'Members connected',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.w800,
                                                        fontSize: 17),
                                                  ),
                                                  SizedBox(
                                                      height:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .height *
                                                              0.02),
                                                  Wrap(
                                                    spacing: 15,
                                                    children: [
                                                      Container(
                                                        width: 55,
                                                        height: 55,
                                                        decoration: const BoxDecoration(
                                                            shape:
                                                                BoxShape.circle,
                                                            image: DecorationImage(
                                                                image: NetworkImage(
                                                                    'https://i.pinimg.com/originals/89/ba/ea/89baead9a793409062027fc707f5874a.jpg'),
                                                                fit: BoxFit
                                                                    .fill)),
                                                      ),
                                                      Container(
                                                        width: 55,
                                                        height: 55,
                                                        decoration: const BoxDecoration(
                                                            shape:
                                                                BoxShape.circle,
                                                            image: DecorationImage(
                                                                image: NetworkImage(
                                                                    'https://i.pinimg.com/originals/89/ba/ea/89baead9a793409062027fc707f5874a.jpg'),
                                                                fit: BoxFit
                                                                    .fill)),
                                                      ),
                                                      Container(
                                                        width: 55,
                                                        height: 55,
                                                        decoration: const BoxDecoration(
                                                            shape:
                                                                BoxShape.circle,
                                                            image: DecorationImage(
                                                                image: NetworkImage(
                                                                    'https://i.pinimg.com/originals/89/ba/ea/89baead9a793409062027fc707f5874a.jpg'),
                                                                fit: BoxFit
                                                                    .fill)),
                                                      ),
                                                      Container(
                                                        width: 55,
                                                        height: 55,
                                                        decoration:
                                                            const BoxDecoration(
                                                          shape:
                                                              BoxShape.circle,
                                                        ),
                                                        child:
                                                            FloatingActionButton(
                                                          backgroundColor:
                                                              Colors.grey[300],
                                                          onPressed: () {
                                                            Get.snackbar(
                                                                'berhasil cuy',
                                                                'ahay',
                                                                backgroundColor:
                                                                    Colors
                                                                        .white);
                                                          },
                                                          elevation: 0,
                                                          child: Icon(
                                                            Icons.add,
                                                            color:
                                                                Colors.black54,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  )
                                                ],
                                              ),
                                            ))
                                          ],
                                        ),
                                      ),
                                      bottomNavigationBar: BottomNavigation(),
                                    );
                                  },
                                );
                              },
                              child: Text(
                                'See all',
                                style: TextStyle(
                                    color: Colors.brown[200],
                                    fontWeight: FontWeight.w500),
                              ))
                        ],
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.02,
                      ),
                      Expanded(
                          child: ListView.builder(
                        itemCount: 4,
                        itemBuilder: (context, index) {
                          return Card(
                            elevation: 0,
                            color: Colors.white54,
                            shape: RoundedRectangleBorder(
                                side: BorderSide(color: Colors.grey[300]!),
                                borderRadius: BorderRadius.circular(10)),
                            child: Container(
                              width: MediaQuery.of(context).size.width * 1,
                              height: MediaQuery.of(context).size.height * 0.2,
                              padding: EdgeInsets.all(20),
                              child: Stack(
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Data ${index}',
                                            style: TextStyle(
                                                color: Colors.black54,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          Text(
                                            '${index}h',
                                            style: TextStyle(
                                                color: Colors.black54,
                                                fontWeight: FontWeight.w500),
                                          )
                                        ],
                                      ),
                                      Text(
                                        'Blog and social ${index}',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w900,
                                            fontSize: 20),
                                      ),
                                    ],
                                  ),
                                  Positioned(
                                      bottom: 0,
                                      left: 0,
                                      child: Row(
                                        children: [
                                          Icon(Icons.error_outline),
                                          Padding(
                                            padding: EdgeInsets.only(left: 5),
                                            child: Text('Deadline is today'),
                                          )
                                        ],
                                      ))
                                ],
                              ),
                            ),
                          );
                        },
                      )),
                    ],
                  ),
                ))
              ],
            )),
        bottomNavigationBar: BottomNavigation());
  }
}
