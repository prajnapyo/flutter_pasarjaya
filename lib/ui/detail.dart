import 'package:flutter/material.dart';
import 'package:flutter_learn/model/akun.dart';
import 'package:flutter_learn/service/akun_service.dart';
import 'package:flutter_learn/store/akun_store.dart';

class DetailPage extends StatefulWidget {
  const DetailPage({Key? key, required this.id}) : super(key: key);
  final String id;

  @override
  _DetailPage createState() => _DetailPage(id: id);
}

class _DetailPage extends State<DetailPage> {
  _DetailPage({required this.id});
  final String id;

  @override
  initState() {
    // this is called when the class is initialized or called for the first time
    super.initState();
    getData();
  }

  Future<List<Akun>> getData() async {
    var data = await AkunService().getDetailAkun(id);
    return data;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Detail Akun')),
      body: Container(
        margin: const EdgeInsets.all(20),
        child: FutureBuilder<List<Akun>>(
            future: getData(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                var name =
                    TextEditingController(text: snapshot.data?[0].name ?? '');
                var age = TextEditingController(
                    text: snapshot.data?[0].age.toString() ?? '');
                var alamat =
                    TextEditingController(text: snapshot.data?[0].alamat ?? '');
                return ListView(children: [
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Align(
                              alignment: Alignment.center,
                              child: Text('Identitas Akun',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 28))),
                          const SizedBox(
                            height: 20,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Name: ${snapshot.data![0].name}',
                                  style: const TextStyle(fontSize: 18)),
                              Text('Age: ${snapshot.data![0].age.toString()}',
                                  style: const TextStyle(fontSize: 18)),
                              Text('Name: ${snapshot.data![0].alamat}',
                                  style: const TextStyle(fontSize: 18)),
                            ],
                          ),
                          const Padding(
                            padding: EdgeInsets.only(bottom: 20, top: 20),
                            child: Text(
                              'Ubah Data',
                              style: TextStyle(
                                  fontWeight: FontWeight.w700, fontSize: 20),
                            ),
                          ),
                          Form(
                              child: Column(
                            children: [
                              TextFormField(
                                controller: name,
                                decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.white,
                                    labelText: 'name',
                                    border: InputBorder.none,
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(20),
                                        borderSide:
                                            const BorderSide(color: Colors.blue)),
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(20),
                                        borderSide:
                                            const BorderSide(color: Colors.blue))),
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              TextFormField(
                                controller: age,
                                decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.white,
                                    labelText: 'name',
                                    border: InputBorder.none,
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(20),
                                        borderSide:
                                            const BorderSide(color: Colors.blue)),
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(20),
                                        borderSide:
                                            const BorderSide(color: Colors.blue))),
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              TextFormField(
                                controller: alamat,
                                decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.white,
                                    labelText: 'alamat',
                                    border: InputBorder.none,
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(20),
                                        borderSide:
                                            const BorderSide(color: Colors.blue)),
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(20),
                                        borderSide:
                                            const BorderSide(color: Colors.blue))),
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              ElevatedButton(
                                onPressed: () async {
                                  await AkunService()
                                      .updateAkun(
                                          snapshot.data?[0].id ?? '',
                                          name.text,
                                          int.parse(age.text),
                                          alamat.text)
                                      .then((value) async {
                                    if (value == 1) {
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) =>
                                              const AlertDialog(
                                                title: Icon(
                                                  Icons.check_circle,
                                                  color: Colors.green,
                                                  size: 32,
                                                ),
                                                content: Text(
                                                  'Success Changes Data',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                              ));
                                      setState(() {
                                        getData();
                                      });
                                    } else {
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) =>
                                              const AlertDialog(
                                                title: Icon(
                                                  Icons.error,
                                                  color: Colors.red,
                                                  size: 32,
                                                ),
                                                content: Text(
                                                  'Failed Changes Data',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                              ));
                                    }
                                  });
                                },
                                child: Container(
                                  alignment: Alignment.center,
                                  height: 50,
                                  width: 500,
                                  child: const Text('Submit'),
                                ),
                                style: ElevatedButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(20))),
                              )
                            ],
                          ))
                        ],
                      ),
                    ),
                  )
                ]);
              } else {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
            }),
      ),
    );
  }
}
