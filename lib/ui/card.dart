import 'package:flutter/material.dart';
import 'package:flutter_learn/model/akun.dart';
import 'package:flutter_learn/model/typicode.dart';

class CardPage extends StatefulWidget {

  CardPage({
    required this.item,
    Key? key,
  }) : super(key: key);

  final TypiCode item;

  @override
  _CardPage createState() => _CardPage();
}

class _CardPage extends State<CardPage> {

  @override
  Widget build(BuildContext context) => ListTile(
        title: Text(widget.item.title!),
      );
}
