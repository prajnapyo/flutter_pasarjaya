import 'package:flutter/material.dart';
import 'package:flutter_learn/controller/zoom_drawer_controller.dart';
import 'package:flutter_learn/ui/main_screen.dart';
import 'package:flutter_learn/ui/menu_screen.dart';
import 'package:flutter_zoom_drawer/config.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:get/get.dart';

// class FlutterZoomDrawerDemo extends StatefulWidget {
//
//   @override
//   _FlutterZoomDrawerDemoState createState() => _FlutterZoomDrawerDemoState();
// }
//
// class _FlutterZoomDrawerDemoState extends State<FlutterZoomDrawerDemo> {
//   final _drawerController = ZoomDrawerController();
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: ZoomDrawer(
//         controller: _drawerController,
//         style: DrawerStyle.defaultStyle,
//         menuScreen: MenuScreen(),
//         mainScreen: MainScreen(),
//         borderRadius: 24.0,
//         showShadow: true,
//         angle: 0.0,
//         drawerShadowsBackgroundColor: Colors.grey,
//         slideWidth: MediaQuery.of(context).size.width*.65,
//         openCurve: Curves.fastOutSlowIn,
//         closeCurve: Curves.bounceIn,
//       ),
//     );
//   }
// }

class ZoomDrawerScreen extends GetView<MyZoomDrawer> {
  const ZoomDrawerScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MyZoomDrawer>(
      builder: (_) => ZoomDrawer(
          controller: _.zoomDrawerController,
          style: DrawerStyle.defaultStyle,
          borderRadius: 16.0,
          showShadow: true,
          menuBackgroundColor: Colors.white,
          angle: 0.0,
          slideWidth: MediaQuery.of(context).size.width * .65,
          drawerShadowsBackgroundColor: Colors.grey,
          menuScreen: MenuScreen(),
          mainScreen: MainScreen()),
    );
  }
}
