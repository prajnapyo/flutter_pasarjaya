import 'package:flutter/material.dart';
import 'package:flutter_learn/ui/animation_start_screen.dart';
import 'package:flutter_learn/ui/card.dart';
import 'package:flutter_learn/ui/infinity_scroll.dart';

class HealthScreen extends StatefulWidget {
  @override
  _HealthScreen createState() => _HealthScreen();
}

class _HealthScreen extends State<HealthScreen> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(100),
        child: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          flexibleSpace: Container(
            color: Colors.orange,
            alignment: Alignment.center,
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Hi Ruslandi,',
                        style: TextStyle(color: Colors.white),
                      ),
                      Text(
                        'Welcome Back!',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 28,
                            fontWeight: FontWeight.w600),
                      )
                    ],
                  ),
                ),
                FloatingActionButton(
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => AnimationStart(),));
                  },
                  child: Icon(
                    Icons.notifications_none,
                    size: 30,
                  ),
                  heroTag: "btn3",
                  elevation: 0,
                  backgroundColor: Colors.transparent,
                  shape: RoundedRectangleBorder(
                      side: BorderSide(width: 0.5, color: Colors.white),
                      borderRadius: BorderRadius.circular(50)),
                )
              ],
            ),
          ),
        ),
      ),
      body: Container(
        color: Colors.orange,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(left: 30, right: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                      flex: 4,
                      child: TextField(
                        decoration: InputDecoration(
                            filled: true,
                            contentPadding: EdgeInsets.all(17),
                            fillColor: Colors.white,
                            prefixIcon: Icon(Icons.search, color: Colors.grey,),
                            hintText: 'Search...',
                            hintStyle: TextStyle(color: Colors.grey),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(10)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(10))),
                      )),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.05,
                  ),
                  Expanded(
                      flex: 1,
                      child: FloatingActionButton(
                        heroTag: "btn10",
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => InfinityScrollPage(),));
                        },
                        backgroundColor: Colors.white,
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            side: BorderSide.none,
                            borderRadius: BorderRadius.circular(10)),
                        child: Icon(
                          Icons.filter_list,
                          color: Colors.black,
                        ),
                      ))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
