import 'package:flutter/material.dart';
import 'package:flutter_learn/controller/akun_controller.dart';
import 'package:flutter_learn/controller/typicode_controller.dart';
import 'package:flutter_learn/model/akun.dart';
import 'package:flutter_learn/model/typicode.dart';
import 'package:flutter_learn/ui/card.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class InfinityScrollPage extends StatefulWidget {
  const InfinityScrollPage({Key? key}) : super(key: key);

  @override
  _InfinityScrollPage createState() => _InfinityScrollPage();
}

class _InfinityScrollPage extends State<InfinityScrollPage> {
  final controller = Get.find<TypiCodeController>();
  String nama = "gila";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: RefreshIndicator(
          onRefresh: () => Future.sync(() => controller.refresh()),
          child:
            PagedListView(
                pagingController: controller.getPagingController,
                builderDelegate: PagedChildBuilderDelegate<TypiCode>(
                  itemBuilder: (context, item, index) {
                    print(item.title!);
                    return CardPage(item: item,);
                  },
                ))
          ),
      ),
    );
  }
}
