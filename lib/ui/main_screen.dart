import 'package:flutter/material.dart';
import 'package:flutter_learn/controller/zoom_drawer_controller.dart';
import 'package:get/get.dart';

class MainScreen extends GetView<MyZoomDrawer> {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.pink,
      child: Center(
        child: ElevatedButton(
          onPressed: controller.toggleDrawer,
          child: Text("Toggle Drawer"),
        ),
      ),
    );
  }
}