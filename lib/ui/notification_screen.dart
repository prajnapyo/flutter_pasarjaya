import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({Key? key}) : super(key: key);

  @override
  _NotificationScreen createState() => _NotificationScreen();
}

class _NotificationScreen extends State<NotificationScreen> {
  bool choose = false;
  bool edit = false;
  bool selectedList = false;
  List multipleSelected = [];
  List checkListItems = [
    {
      "id": 0,
      "value": false,
      "pembayaran": "berhasil",
      "title": "Sunday",
    },
    {
      "id": 1,
      "value": false,
      "pembayaran": "berhasil",
      "title": "Monday",
    },
    {
      "id": 2,
      "value": false,
      "pembayaran": "berhasil",
      "title": "Tuesday",
    },
    {
      "id": 3,
      "value": false,
      "pembayaran": "berhasil",
      "title": "Wednesday",
    },
    {
      "id": 4,
      "value": false,
      "pembayaran": "berhasil",
      "title": "Thursday",
    },
    {
      "id": 5,
      "value": false,
      "pembayaran": "gagal",
      "title": "Friday",
    },
    {
      "id": 6,
      "value": false,
      "pembayaran": "gagal",
      "title": "Saturday",
    },
    {
      "id": 6,
      "value": false,
      "pembayaran": "gagal",
      "title": "Saturday",
    },
    {
      "id": 7,
      "value": false,
      "pembayaran": "gagal",
      "title": "Saturday",
    },
    {
      "id": 8,
      "value": false,
      "pembayaran": "berhasil",
      "title": "Saturday",
    },
    {
      "id": 9,
      "value": true,
      "pembayaran": "gagal",
      "title": "Saturday",
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        elevation: 0,
        shape: RoundedRectangleBorder(
            side: BorderSide(color: Color(0xff6E85B7), width: 0.5)),
        flexibleSpace: Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.only(left: 30, right: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              AnimatedCrossFade(
                firstCurve: Curves.linear,
                secondCurve: Curves.linear,
                crossFadeState:
                    edit ? CrossFadeState.showFirst : CrossFadeState.showSecond,
                duration: Duration(milliseconds: 300),
                firstChild: Text(
                  'Edit Notifikasi',
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Plus Jakarta Sans',
                      fontSize: 20,
                      color: Color(0xff092C4C)),
                ),
                secondChild: Text(
                  'Notifikasi',
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 20,
                      fontFamily: 'Plus Jakarta Sans',
                      color: Color(0xff092C4C)),
                ),
              ),
              IconButton(
                onPressed: () {
                  setState(() {
                    edit = !edit;
                  });
                },
                icon: AnimatedCrossFade(
                  firstCurve: Curves.linear,
                  secondCurve: Curves.linear,
                  crossFadeState: edit
                      ? CrossFadeState.showFirst
                      : CrossFadeState.showSecond,
                  duration: Duration(milliseconds: 300),
                  firstChild: Icon(Icons.close),
                  secondChild: SvgPicture.asset('assets/images/edit-2.svg'),
                ),
              )
            ],
          ),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(top: 10),
        color: Colors.white,
        child: Stack(
          children: [
            AnimatedPositioned(
              top: edit ? 0 : -60,
              left: 15,
              curve: Curves.easeIn,
              duration: const Duration(milliseconds: 300),
              child: Row(
                children: [
                  Checkbox(
                    activeColor: Color(0xff6E85B8),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    value: choose,
                    onChanged: (value) {
                      setState(() {
                        choose = value!;
                        multipleSelected.clear();
                        for (var element in checkListItems) {
                          if (choose == true) {
                            element["value"] = true;
                            multipleSelected.add(element);
                          } else {
                            element["value"] = false;
                            multipleSelected.remove(element);
                          }
                        }
                      });
                    },
                  ),
                  Text(
                    'Pilih Semua',
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontFamily: 'Plus Jakarta Sans',
                        fontSize: 18,
                        color: Color(0xff092C4C)),
                  )
                ],
              ),
            ),
            AnimatedContainer(
              margin: edit
                  ? EdgeInsets.only(top: 50, bottom: 120)
                  : EdgeInsets.only(bottom: 20),
              padding: edit
                  ? EdgeInsets.only(left: 70, right: 0)
                  : EdgeInsets.only(right: 0),
              curve: Curves.linear,
              transform: Matrix4.translationValues(-40.0, 0.0, 0.0),
              duration: Duration(milliseconds: 300),
              child: ListView.builder(
                physics: BouncingScrollPhysics(),
                itemCount: checkListItems.length,
                itemBuilder: (context, index) {
                  return Row(
                    children: [
                      AnimatedContainer(
                        duration: Duration(milliseconds: 300),
                        width: edit ? 20 : 70,
                        height:  edit ? 20 : 70,
                        child: Checkbox(
                            activeColor: Color(0xff6E85B8),
                            onChanged: (value) {
                              setState(() {
                                checkListItems[index]["value"] = value;
                              });
                            },
                            value: checkListItems[index]["value"],
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5))),
                      ),
                      Expanded(
                          child: ListTile(contentPadding: EdgeInsets.only(left: 20, right: 0) ,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        onLongPress: () {
                          setState(() {
                            edit = !edit;
                            checkListItems[index]["value"] =
                            !checkListItems[index]["value"];
                          });
                        },
                        onTap: () {
                          setState(() {
                            edit ?
                            checkListItems[index]["value"] =
                                !checkListItems[index]["value"] : null;
                          });
                        },
                        leading: Container(
                          width: 50,
                          height: 50,
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              color:
                                  checkListItems[index]["pembayaran"] == "gagal"
                                      ? Color(0xffE4403F).withOpacity(0.15)
                                      : Color(0xff65DC41).withOpacity(0.15)),
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(25),
                                border: Border.all(
                                    color: checkListItems[index]
                                                ["pembayaran"] ==
                                            "gagal"
                                        ? Color(0xffE4403F)
                                        : Color(0xff65DC41), width: 1.5)),
                            child: Badge(
                              position: BadgePosition.topStart(top: -10, start: -30),
                              badgeColor: edit ? Colors.transparent : checkListItems[index]["value"] ? Color(0xff6F85B8) : Colors.transparent,
                              elevation: 0,
                              child: Icon(
                                checkListItems[index]["pembayaran"] == "gagal"
                                    ? Icons.close
                                    : Icons.check_rounded,
                                color: checkListItems[index]["pembayaran"] ==
                                        "gagal"
                                    ? Color(0xffE4403F)
                                    : Color(0xff65DC41),
                                size: 20,
                              ),
                            ),
                          ),
                        ),
                        title: Text(
                          'Pembayaran ${checkListItems[index]["pembayaran"]}',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontFamily: 'Plus Jakarta Sans',
                              color: Color(0xff092C4C)),
                        ),
                        subtitle: Container(
                          padding: EdgeInsets.only(bottom: 10),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                            color: Colors.black12,
                          ))),
                          child: Text(
                            '19 0ctober 2022',
                            style: TextStyle(
                              fontFamily: 'Plus Jakarta Sans',
                            ),
                          ),
                        ),
                      )),
                    ],
                  );
                },
              ),
            ),
            AnimatedPositioned(
              bottom: edit ? 0 : -100,
              duration: Duration(milliseconds: 300),
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(top: 20, bottom: 20),
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey.withOpacity(0.1),
                        spreadRadius: 1,
                        blurRadius: 7,),
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(
                      onPressed: () {

                      },
                      style: ElevatedButton.styleFrom(
                        elevation: 0,
                        disabledForegroundColor: Colors.red,
                        backgroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.black12, width: 2),
                            borderRadius: BorderRadius.circular(10)
                        ),
                        padding: EdgeInsets.all(0),
                      ),
                      child: Container(
                        width: 175,
                        height: 60,
                        alignment: Alignment.center,
                        child:
                          Text(
                            'Telah Dibaca',
                            style: TextStyle(
                                color: Color(0xff6E85B7),
                                fontFamily: 'Plus Jakarta Sans',
                                fontWeight: FontWeight.w700),
                          ),
                      ),
                    ),
                    ElevatedButton(
                      onPressed: () {

                      },
                      style: ElevatedButton.styleFrom(
                        elevation: 0,
                        backgroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.red.shade100,width: 2),
                            borderRadius: BorderRadius.circular(10)
                        ),
                        padding: EdgeInsets.all(0),
                      ),
                      child: Container(
                        width: 175,
                        height: 60,
                        alignment: Alignment.center,
                        child:  Text(
                            'Hapus',
                            style: TextStyle(
                                color: Colors.red, fontWeight: FontWeight.w700, fontFamily: 'Plus Jakarta Sans'),
                          ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
