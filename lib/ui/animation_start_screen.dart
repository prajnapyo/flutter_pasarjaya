import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/countdown_timer_controller.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter_learn/ui/menu_screen.dart';
import 'package:flutter_learn/ui/notifications_screen.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class AnimationStart extends StatefulWidget {
  const AnimationStart({Key? key}) : super(key: key);

  @override
  State<AnimationStart> createState() => _AnimationStart();
}

class _AnimationStart extends State<AnimationStart> {
  double turns = 0.0;
  double turns2 = 0.25;
  int page = 0;
  bool isExpanded = false;
  Timer? _timer;
  int _start = 10;

  CountdownTimerController? controller;
  int endTime = DateTime.now().millisecondsSinceEpoch + 1000 * 30;

  @override
  void initState() {
    super.initState();
    controller = CountdownTimerController(endTime: endTime, onEnd: onEnd);
  }

  void onEnd() {
    print('onEnd');
  }

  @override
  void dispose() {
    controller!.dispose();
    super.dispose();
  }

  Widget Screen1() {
    return Container(
      child: ExpansionPanelList(
        expansionCallback: (panelIndex, w) {
          setState(() {
            isExpanded = !isExpanded;
          });
        },
        children: [
          ExpansionPanel(
              canTapOnHeader: true,
              headerBuilder: (context, isExpanded) {
                return Container(
                  child: Text('sss'),
                );
              },
              body: Container(
                child: Text('www'),
              ),
              isExpanded: isExpanded)
        ],
      ),
    );
  }

  Widget Screen2() {
    return Row(
      children: [
        CountdownTimer(
          controller: controller,
          onEnd: onEnd,
          widgetBuilder: (context, time) {
            if (time == null) {
              return Text('yahh habis');
            }
            return Text('Tunggu 00:${time!.sec} untuk kirim ulang');
          },
          endTime: endTime,
        ),
        FloatingActionButton(
          child: Icon(Icons.stop),
          onPressed: () {
            onEnd();
            controller?.disposeTimer();
          },
        ),
      ],
    );
  }

  Widget Screen3() {
    return Text(
      'Page 3',
      style: GoogleFonts.plusJakartaSans(),
    );
  }

  late List<Widget> screen = [Screen1(), Screen2(), Screen3()];

  @override
  Widget build(BuildContext context) {
    print(page);
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(10),
        child: Column(children: [
          SizedBox(
            height: 100,
          ),
          screen[page],
          ExpansionPanelList(
            expansionCallback: (panelIndex, w) {
              setState(() {
                isExpanded = !isExpanded;
              });
            },
            children: [
              ExpansionPanel(
                  canTapOnHeader: true,
                  headerBuilder: (context, isExpanded) {
                    return Container(
                      child: Text('sss'),
                    );
                  },
                  body: Container(
                    child: Text('www'),
                  ),
                  isExpanded: isExpanded)
            ],
          ),
          Spacer(),
          AnimatedContainer(
              duration: Duration(milliseconds: 500),
              width: MediaQuery.of(context).size.width,
              alignment: page == 0
                  ? Alignment.centerLeft
                  : page == 1
                      ? Alignment.center
                      : Alignment.centerRight,
              height: 5,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(2.5),
                  color: Colors.black),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.33,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(2.5),
                  color: Colors.red,
                ),
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextButton(
                  onPressed: () {
                    setState(() {
                      page = 0;
                    });
                  },
                  child: Text('skip')),
              CircularPercentIndicator(
                radius: 30,
                lineWidth: 5,
                animateFromLastPercent: true,
                animation: true,
                animationDuration: 500,
                circularStrokeCap: CircularStrokeCap.round,
                percent: page == 0
                    ? 0.3
                    : page == 1
                        ? 0.7
                        : 1,
                center: AnimatedRotation(
                    turns: page >= 2 ? turns : turns2,
                    duration: Duration(milliseconds: 500),
                    child: Icon(
                      Icons.chevron_right,
                      size: 30,
                    )),
              ),
              TextButton(
                  onPressed: () {
                    setState(() {
                      if (page < 2) {
                        page++;
                      } else {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => MyHomePage(),
                            ));
                      }
                    });
                  },
                  child: Text('Next'))
            ],
          )
        ]),
      ),
    );
  }
}
