import 'package:flutter/material.dart';
import 'package:flutter_learn/store/akun_store.dart';
import 'package:get_it/get_it.dart';

class Konfirmasi extends StatefulWidget {
  const Konfirmasi({Key? key}) : super(key: key);

  @override
  State<Konfirmasi> createState() => _Konfirmasi();
}

class _Konfirmasi extends State<Konfirmasi> {
  final AkunStore akun = GetIt.instance.get<AkunStore>();

  @override
  Widget build(BuildContext context) {
    final mediaQueryHeight = MediaQuery.of(context).size.height;
    final mediaQueryWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(title: Text('Konfirmasi'),),
      body: Container(
        color: Colors.white,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(10),
              width: mediaQueryWidth,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20)
              ),
              child: Card(
                child: Column(
                  children: [
                    Text(akun.name!),
                    Text(akun.age!.toString()),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
