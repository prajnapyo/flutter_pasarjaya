import 'package:flutter/material.dart';
import 'package:flutter_learn/home.dart';
import 'package:flutter_learn/service/akun_service.dart';
import 'package:flutter_learn/store/akun_store.dart';
import 'package:flutter_learn/ui/konfirmasi.dart';
import 'package:get_it/get_it.dart';

class TambahPage extends StatefulWidget {
  const TambahPage({Key? key}) : super(key: key);

  @override
  State<TambahPage> createState() => _TambahPage();
}

class _TambahPage extends State<TambahPage> {
  final _formKey = GlobalKey<FormState>();
  final AkunStore akun = GetIt.instance.get<AkunStore>();
  final name = TextEditingController();
  final age = TextEditingController();
  final alamat = TextEditingController();

  void tambah(context) async {
    if (_formKey.currentState!.validate()) {
      akun.addAkun(name.text, int.parse(age.text), alamat.text);
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return const Konfirmasi();
      },));
    } else {
      setState(() {

      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Text('Tambah Akun'),
        ),
        body: Container(
          margin: const EdgeInsets.all(10),
          child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextFormField(
                    controller: name,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        labelText: 'name',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: const BorderSide(color: Colors.blue))),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    controller: age,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        labelText: 'age',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: const BorderSide(color: Colors.blue))),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    controller: alamat,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        labelText: 'alamat',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: const BorderSide(color: Colors.blue))),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      tambah(context);
                    },
                    style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20))),
                    child: Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: 500,
                      child: const Text('Lanjutkan'),
                    ),
                  )
                ],
              )),
        ));
  }
}
