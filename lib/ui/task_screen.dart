import 'package:flutter/material.dart';
import 'package:flutter_learn/controller/navigation_controller.dart';
import 'package:flutter_learn/home.dart';
import 'package:flutter_learn/ui/bottom_navigation.dart';
import 'package:flutter_learn/ui/home_screen.dart';
import 'package:flutter_learn/ui/notification_screen.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class TaskScreen extends StatefulWidget {
  const TaskScreen({Key? key}) : super(key: key);

  @override
  _TaskScreen createState() => _TaskScreen();
}

class _TaskScreen extends State<TaskScreen> {
  final controller = Get.find<NavigationController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: const Size.fromHeight(100),
          child: AppBar(
            elevation: 0,
            automaticallyImplyLeading: false,
            backgroundColor: const Color(0xffd1edbf),
            flexibleSpace: Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.only(
                  left: 20, right: 20),
              child: Row(
                mainAxisAlignment:
                MainAxisAlignment
                    .spaceBetween,
                children: [
                  Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(
                            color: Colors.grey,
                            width: 1,
                            style: BorderStyle
                                .solid)),
                    child: FloatingActionButton(
                      heroTag: "btn1",
                      elevation: 0,
                      backgroundColor:
                      const Color(0xffd1edbf),
                      onPressed: () {
                        Navigator.pop(context);
                        controller.onTabTapped(0);
                      },
                      child: const Icon(
                        Icons.close,
                        color: Colors.black,
                        size: 30,
                      ),
                    ),
                  ),
                  IconButton(
                      onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) {
                          return Home();
                        },));
                      },
                      icon: const Icon(
                        Icons.more_horiz,
                        size: 30,
                      ))
                ],
              ),
            ),
          )),
      body: Container(
        width:
        MediaQuery.of(context).size.width,
        height:
        MediaQuery.of(context).size.height,
        color: const Color(0xffd1edbf),
        child: Column(
          crossAxisAlignment:
          CrossAxisAlignment.start,
          children: [
            Expanded(
                child: Container(
                  width: MediaQuery.of(context)
                      .size
                      .width,
                  padding: const EdgeInsets.only(
                      left: 30, right: 30, top: 60),
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius:
                      BorderRadius.only(
                          topLeft:
                          Radius.circular(
                              50),
                          topRight:
                          Radius.circular(
                              50))),
                  child: Column(
                    crossAxisAlignment:
                    CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Messaging ID',
                        style: TextStyle(
                            fontSize: 30,
                            fontWeight:
                            FontWeight.w900),
                      ),
                      SizedBox(
                        height:
                        MediaQuery.of(context)
                            .size
                            .height *
                            0.03,
                      ),
                      Row(
                        mainAxisAlignment:
                        MainAxisAlignment
                            .spaceBetween,
                        children: const [
                          Text(
                            'Your daily plan',
                            style: TextStyle(
                                fontWeight:
                                FontWeight.w800,
                                fontSize: 17),
                          ),
                          Text('70%',
                              style: TextStyle(
                                  fontWeight:
                                  FontWeight
                                      .w800,
                                  fontSize: 17))
                        ],
                      ),
                      SizedBox(
                        height:
                        MediaQuery.of(context)
                            .size
                            .height *
                            0.015,
                      ),
                      LinearPercentIndicator(
                        percent: 0.7,
                        barRadius: const Radius.circular(20),
                        progressColor: Colors.black,
                        lineHeight: 10,
                      ),
                      SizedBox(
                        height:
                        MediaQuery.of(context)
                            .size
                            .height *
                            0.015,
                      ),
                      const Text(
                        '4 of 6 completed',
                        style: TextStyle(
                            color: Colors.grey,
                            fontWeight:
                            FontWeight.w600),
                      ),
                      SizedBox(
                        height:
                        MediaQuery.of(context)
                            .size
                            .height *
                            0.03,
                      ),
                      Row(
                        mainAxisAlignment:
                        MainAxisAlignment
                            .spaceBetween,
                        children: [
                          Card(
                            color:
                            const Color(0xffffe7ab),
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius
                                    .circular(
                                    10)),
                            elevation: 0,
                            child: Container(
                              width: 165,
                              padding:
                              const EdgeInsets.all(
                                  15),
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment
                                    .start,
                                children: [
                                  const Text(
                                    '17',
                                    style: TextStyle(
                                        fontWeight:
                                        FontWeight
                                            .w600,
                                        fontSize:
                                        30),
                                  ),
                                  Row(
                                    children: const [
                                      Icon(Icons
                                          .task),
                                      Text(
                                        ' Task finished',
                                        style: TextStyle(
                                            color: Colors
                                                .black54),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                          Card(
                            color:
                            const Color(0xffffe7ab),
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius
                                    .circular(
                                    10)),
                            elevation: 0,
                            child: Container(
                              width: 165,
                              padding:
                              const EdgeInsets.all(
                                  15),
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment
                                    .start,
                                children: [
                                  const Text(
                                    '3,2',
                                    style: TextStyle(
                                        fontWeight:
                                        FontWeight
                                            .w600,
                                        fontSize:
                                        30),
                                  ),
                                  Row(
                                    children: const [
                                      Icon(Icons
                                          .lock_clock),
                                      Text(
                                          ' Tracked hours',
                                          style: TextStyle(
                                              color:
                                              Colors.black54))
                                    ],
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height:
                        MediaQuery.of(context)
                            .size
                            .height *
                            0.03,
                      ),
                      const Text(
                        'Overview',
                        style: TextStyle(
                            fontWeight:
                            FontWeight.w800,
                            fontSize: 17),
                      ),
                      SizedBox(
                          height:
                          MediaQuery.of(context)
                              .size
                              .height *
                              0.02),
                      const Text(
                        'Messaging ID framework development for marketing branch and the publicity bureau and implemented draft on the framework',
                        style: TextStyle(
                            color: Colors.black54),
                      ),
                      SizedBox(
                          height:
                          MediaQuery.of(context)
                              .size
                              .height *
                              0.05),
                      const Text(
                        'Members connected',
                        style: TextStyle(
                            fontWeight:
                            FontWeight.w800,
                            fontSize: 17),
                      ),
                      SizedBox(
                          height:
                          MediaQuery.of(context)
                              .size
                              .height *
                              0.02),
                      Wrap(
                        spacing: 15,
                        children: [
                          Container(
                            width: 55,
                            height: 55,
                            decoration: const BoxDecoration(
                                shape:
                                BoxShape.circle,
                                image: DecorationImage(
                                    image: NetworkImage(
                                        'https://i.pinimg.com/originals/89/ba/ea/89baead9a793409062027fc707f5874a.jpg'),
                                    fit: BoxFit
                                        .fill)),
                          ),
                          Container(
                            width: 55,
                            height: 55,
                            decoration: const BoxDecoration(
                                shape:
                                BoxShape.circle,
                                image: DecorationImage(
                                    image: NetworkImage(
                                        'https://i.pinimg.com/originals/89/ba/ea/89baead9a793409062027fc707f5874a.jpg'),
                                    fit: BoxFit
                                        .fill)),
                          ),
                          Container(
                            width: 55,
                            height: 55,
                            decoration: const BoxDecoration(
                                shape:
                                BoxShape.circle,
                                image: DecorationImage(
                                    image: NetworkImage(
                                        'https://i.pinimg.com/originals/89/ba/ea/89baead9a793409062027fc707f5874a.jpg'),
                                    fit: BoxFit
                                        .fill)),
                          ),
                          Container(
                            width: 55,
                            height: 55,
                            decoration:
                            const BoxDecoration(
                              shape:
                              BoxShape.circle,
                            ),
                            child:
                            FloatingActionButton(
                              heroTag: "btn2",
                              backgroundColor:
                              Colors.grey[300],
                              onPressed: () {
                                Navigator.push(context, MaterialPageRoute(builder: (context) {
                                  return NotificationScreen();
                                },));
                              },
                              elevation: 0,
                              child: const Icon(
                                Icons.add,
                                color:
                                Colors.black54,
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ))
          ],
        ),
      ),
      bottomNavigationBar: const BottomNavigation(),
    );
  }
}
