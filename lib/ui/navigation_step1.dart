import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_learn/ui/home.dart';
import 'package:flutter_learn/ui/konfirmasi.dart';
import 'package:flutter_learn/ui/management_screen.dart';
import 'package:flutter_learn/ui/product.dart';
import 'package:flutter_learn/ui/tambah.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';

class NavigationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
      context,
      screens: _buildScreen(),
      items: _navBarsItems(),
      navBarStyle: NavBarStyle.style15,
    );
  }

  List<Widget> _buildScreen() {
    return [HealthScreen(), Card(), HomePage(), ProductPage(), TambahPage()];
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
          icon: const Icon(Icons.home),
          activeColorPrimary: CupertinoColors.activeBlue,
          inactiveColorPrimary: CupertinoColors.systemGrey),
      PersistentBottomNavBarItem(
          icon: const Icon(Icons.add),
          activeColorPrimary: CupertinoColors.activeBlue,
          inactiveColorPrimary: CupertinoColors.systemGrey),
      PersistentBottomNavBarItem(
          icon: Stack(children: [
            Container(height: 70, width: 70, color: Colors.red,)
          ],)),
      PersistentBottomNavBarItem(
          icon: const Icon(Icons.add),
          activeColorPrimary: CupertinoColors.activeBlue,
          inactiveColorPrimary: CupertinoColors.systemGrey),
      PersistentBottomNavBarItem(
          icon: Icon(Icons.account_balance),
          activeColorPrimary: CupertinoColors.activeBlue,
          inactiveColorPrimary: CupertinoColors.systemGrey),
    ];
  }
}
