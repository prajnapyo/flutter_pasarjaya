import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_learn/constants.dart';

class ProductPage extends StatefulWidget {
  const ProductPage({Key? key}) : super(key: key);

  @override
  _ProductPage createState() => _ProductPage();
}

class _ProductPage extends State<ProductPage> {
  late PageController _pageController = PageController();
  int activePage = 1;
  int gender = 0;

  List<String> images = [
    "https://i.pinimg.com/564x/0b/40/eb/0b40eb77580331c289c38ff612a4d63f.jpg",
    "https://i.pinimg.com/originals/3c/39/7a/3c397a110bed100bf40ccd76ad94c922.jpg",
    "https://i.pinimg.com/564x/3b/fa/63/3bfa634044a491a0ede61ab9f60852cd.jpg"
  ];

  @override
  void initState() {
    super.initState();
    _pageController = PageController(viewportFraction: 0.8, initialPage: 1);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(70),
        child: AppBar(
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20))),
          title: const Text('Product'),
          centerTitle: true,
          backgroundColor: primaryColor,
          actions: [IconButton(onPressed: () {}, icon: Icon(Icons.more_vert))],
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10,
          ),
          SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 300,
              child: PageView.builder(
                itemCount: images.length,
                controller: _pageController,
                onPageChanged: (value) {
                  setState(() {
                    activePage = value;
                  });
                },
                itemBuilder: (context, index) {
                  bool active = index == activePage;
                  return slider(images, index, active);
                },
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: indicators(images.length, activePage),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(45, 20, 45, 20),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                'Cool Unisex Casual Shoes',
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 20),
                textAlign: TextAlign.start,
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      Icon(
                        Icons.label,
                        color: primaryColor,
                      ),
                      Text(' Rp200.00')
                    ],
                  ),
                  Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      Icon(
                        Icons.favorite,
                        color: Colors.red.shade800,
                      ),
                      Text(' 20k Likes')
                    ],
                  ),
                  Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      Icon(
                        Icons.star,
                        color: Colors.yellow,
                      ),
                      Text(' 4.5 Rates')
                    ],
                  )
                ],
              ),
              Padding(
                padding: EdgeInsets.only(top: 20, bottom: 15),
                child: Text(
                    'Cool Shoes with Casual Style, Suitable for Men and Women. Can be used for Traveling, Marathon, etc.'),
              ),
              Wrap(
                spacing: 20,
                runSpacing: 10,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(Icons.check_circle, color: primaryColor),
                      Text(' Custom Size')
                    ],
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(Icons.check_circle, color: primaryColor),
                      Text(' Guaranted')
                    ],
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(Icons.check_circle, color: primaryColor),
                      Text(' Custom Shoelace')
                    ],
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(Icons.check_circle, color: primaryColor),
                      Text(' Free Shipping')
                    ],
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(Icons.check_circle, color: primaryColor),
                      Text(' Comfortable')
                    ],
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(Icons.check_circle, color: primaryColor),
                      Text(' Custom Shoelace')
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Text(
                    'Size',
                    style: TextStyle(fontWeight: FontWeight.w700),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  CupertinoSlidingSegmentedControl(
                    backgroundColor: Colors.white38,
                    thumbColor: primaryColor,
                    groupValue: gender,
                    children: <int, Widget>{
                      0: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                        child: Text(
                          'XS',
                          style: TextStyle(
                              color: gender == 0 ? Colors.white : primaryColor),
                        ),
                      ),
                      1: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                        child: Text(
                          'S',
                          style: TextStyle(
                              color: gender == 1 ? Colors.white : primaryColor),
                        ),
                      ),
                      2: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                        child: Text(
                          'M',
                          style: TextStyle(
                              color: gender == 2 ? Colors.white : primaryColor),
                        ),
                      ),
                      3: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                        child: Text(
                          'L',
                          style: TextStyle(
                              color: gender == 3 ? Colors.white : primaryColor),
                        ),
                      ),
                      4: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                        child: Text(
                          'XL',
                          style: TextStyle(
                              color: gender == 4 ? Colors.white : primaryColor),
                        ),
                      ),
                      5: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                        child: Text(
                          'XLL',
                          style: TextStyle(
                              color: gender == 5 ? Colors.white : primaryColor),
                        ),
                      ),
                    },
                    onValueChanged: (value) {
                      setState(() {
                        gender = int.tryParse(value.toString())!;
                      });
                    },
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 40,
                    height: 40,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: primaryColor,
                    ),
                    child: Icon(Icons.remove, size: 18, color: Colors.white),
                  ),
                  Container(
                    alignment: Alignment.center,
                    width: 40,
                    height: 40,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(color: primaryColor, width: 2)),
                    child: const Text('1', style: TextStyle(color: primaryColor),),
                  ),
                  Container(
                    width: 40,
                    height: 40,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: primaryColor,
                    ),
                    child: Icon(Icons.add, size: 18, color: Colors.white),
                  ),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: primaryColor,
                          fixedSize: Size(180, 40)),
                      onPressed: () {},
                      child: Text('Order Now'))
                ],
              )
            ]),
          )
        ],
      ),
    );
  }
}

AnimatedContainer slider(images, pagePosition, active) {
  double margin = active ? 5 : 15;

  return AnimatedContainer(
    duration: Duration(milliseconds: 500),
    margin: EdgeInsets.all(margin),
    decoration: BoxDecoration(
        image: DecorationImage(
            image: NetworkImage(images[pagePosition]), fit: BoxFit.cover)),
  );
}

List<Widget> indicators(imagesLength, currentIndex) {
  return List<Widget>.generate(imagesLength, (index) {
    return Container(
      margin: EdgeInsets.all(3),
      width: 5,
      height: 5,
      decoration: BoxDecoration(
          color: currentIndex == index ? Colors.black : Colors.black26,
          shape: BoxShape.circle),
    );
  });
}
