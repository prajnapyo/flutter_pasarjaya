import 'package:flutter/material.dart';
import 'package:flutter_learn/ui/product.dart';
import 'package:flutter_learn/widgets/widgets.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePage createState() => _HomePage();
}

class _HomePage extends State<HomePage> {
  Widget categories(IconData icon, String title) {
    return Column(
      children: [
        ElevatedButton(
          onPressed: () {},
          style: ElevatedButton.styleFrom(
              padding: EdgeInsets.all(10),
              shape: CircleBorder(),
              backgroundColor: Color(0xffa26de6)),
          child: Icon(
            icon,
            size: 35,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          title,
          style: const TextStyle(fontWeight: FontWeight.w500),
        )
      ],
    );
  }

  Widget card(String src, String title, int harga) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (BuildContext context) {
          return ProductPage();
        }));
      },
      child: Column(children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: Image.network(
            src,
            width: 100,
            height: 100,
            fit: BoxFit.cover,
          ),
        ),
        Text(title),
        Text(harga.toString())
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(80),
        child: AppBar(
          flexibleSpace: Container(
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: Colors.white),
                  child: const Icon(
                    Icons.menu_rounded,
                    size: 30,
                    color: Color(0xffa26de6),
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.5,
                  child: TextField(
                    decoration: InputDecoration(
                        hintText: 'Product, Store, etc',
                        hintStyle: const TextStyle(color: Color(0xffa26de6)),
                        isDense: true, // important line
                        contentPadding: const EdgeInsets.fromLTRB(10, 5, 5, 0),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                            borderSide: BorderSide.none),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                            borderSide: const BorderSide(
                              color: Color(0xffceb3ed),
                            )),
                        filled: true,
                        fillColor: Colors.white,
                        suffixIcon: IconButton(
                            onPressed: () {
                              Navigator.push(context, MaterialPageRoute(
                                builder: (context) {
                                  return Home();
                                },
                              ));
                            },
                            icon: const Icon(
                              Icons.search,
                              color: Color(0xffa26de6),
                            ))),
                  ),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(25),
                  child: Image.network(
                    'https://i.pinimg.com/originals/8b/90/b7/8b90b78148bab665c31fb4d66a616718.jpg',
                    width: 40,
                    height: 40,
                    fit: BoxFit.cover,
                  ),
                )
              ],
            ),
          ),
          automaticallyImplyLeading: false,
          backgroundColor: const Color(0xffa26de6),
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(25),
                  bottomRight: Radius.circular(25))),
        ),
      ),
      body: ListView(children: [
        Container(
            margin: const EdgeInsets.all(30),
            alignment: Alignment.center,
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Color(0xffa26de6),
                  ),
                  padding: const EdgeInsets.fromLTRB(10, 20, 10, 20),
                  margin: const EdgeInsets.only(bottom: 30),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                                height: 40,
                                width: 40,
                                margin: const EdgeInsets.only(right: 20),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    color: Colors.white),
                                child: Icon(
                                  Icons.wallet_rounded,
                                  color: Color(0xffa26de6),
                                  size: 30,
                                )),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: const [
                                Text(
                                  "My Wallet",
                                  style: TextStyle(color: Colors.white),
                                ),
                                Text(
                                  "200,00",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ],
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                                height: 40,
                                width: 40,
                                margin: EdgeInsets.only(right: 20),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    color: Colors.white),
                                child: const Icon(
                                  Icons.currency_bitcoin,
                                  color: Color(0xffa26de6),
                                  size: 30,
                                )),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: const [
                                Text(
                                  "My Coins",
                                  style: TextStyle(color: Colors.white),
                                ),
                                Text(
                                  "4.354.678",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ],
                            )
                          ],
                        )
                      ]),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "Categories",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                    ),
                    Row(
                      children: [
                        Text('View More'),
                        Icon(Icons.arrow_right_alt)
                      ],
                    )
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Wrap(
                  runSpacing: 20,
                  spacing: 30,
                  alignment: WrapAlignment.spaceEvenly,
                  children: <Widget>[
                    categories(Icons.sunny, 'All'),
                    categories(Icons.abc, 'Fashion'),
                    categories(Icons.computer, 'Electronic'),
                    categories(Icons.gamepad, 'Game'),
                    categories(Icons.music_note, 'Music'),
                    categories(Icons.chair, 'Furniture'),
                    categories(Icons.car_repair, 'Vehicle'),
                    categories(Icons.more_vert, 'Other')
                  ],
                ),
                const SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "Recommended",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                    ),
                    Row(
                      children: [
                        Text('Explore Now'),
                        Icon(Icons.arrow_right_alt)
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Wrap(
                  spacing: 20,
                  runSpacing: 10,
                  alignment: WrapAlignment.spaceEvenly,
                  children: [
                    card(
                        'https://64.media.tumblr.com/e549050462be310cb7a73b25d76c666c/4920a2e574673401-98/s640x960/94eae44d33b49572bd85d2be2f0d771b8e8d5761.jpg',
                        'aku bang',
                        125000),
                    card(
                        'https://64.media.tumblr.com/e549050462be310cb7a73b25d76c666c/4920a2e574673401-98/s640x960/94eae44d33b49572bd85d2be2f0d771b8e8d5761.jpg',
                        'aku bang',
                        125000),
                    card(
                        'https://64.media.tumblr.com/e549050462be310cb7a73b25d76c666c/4920a2e574673401-98/s640x960/94eae44d33b49572bd85d2be2f0d771b8e8d5761.jpg',
                        'aku bang',
                        125000),
                    card(
                        'https://64.media.tumblr.com/e549050462be310cb7a73b25d76c666c/4920a2e574673401-98/s640x960/94eae44d33b49572bd85d2be2f0d771b8e8d5761.jpg',
                        'aku bang',
                        125000),
                    card(
                        'https://64.media.tumblr.com/e549050462be310cb7a73b25d76c666c/4920a2e574673401-98/s640x960/94eae44d33b49572bd85d2be2f0d771b8e8d5761.jpg',
                        'aku bang',
                        125000),
                    card(
                        'https://64.media.tumblr.com/e549050462be310cb7a73b25d76c666c/4920a2e574673401-98/s640x960/94eae44d33b49572bd85d2be2f0d771b8e8d5761.jpg',
                        'aku bang',
                        125000),
                  ],
                )
              ],
            )),
      ]),
      bottomNavigationBar: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            IconButton(
                onPressed: () {}, icon: Icon(Icons.confirmation_num_sharp)),
            IconButton(
                onPressed: () {}, icon: Icon(Icons.confirmation_num_sharp)),
            Stack(
              alignment: Alignment.topCenter,
              children: [
                Container(
                  transform: Matrix4.translationValues(0, -30, 0),
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [Color(0xff6E85B7), Color(0xffADE2FA)])),
                  child: IconButton(onPressed: () {}, icon: Icon(Icons.add)),
                ),
                Positioned(bottom: 10, child: Text('oke'))
              ],
            ),
            IconButton(
                onPressed: () {}, icon: Icon(Icons.confirmation_num_sharp)),
            IconButton(
                onPressed: () {}, icon: Icon(Icons.confirmation_num_sharp))
          ],
        ),
      ),
    );
  }
}
