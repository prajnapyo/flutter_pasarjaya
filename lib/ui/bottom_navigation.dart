import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_learn/controller/navigation_controller.dart';
import 'package:flutter_learn/ui/home_screen.dart';
import 'package:flutter_learn/ui/management_screen.dart';
import 'package:flutter_learn/ui/product.dart';
import 'package:flutter_learn/ui/start_screen.dart';
import 'package:flutter_learn/ui/tambah.dart';
import 'package:flutter_learn/ui/task_screen.dart';
import 'package:get/get.dart';
import 'package:page_transition/page_transition.dart';

class BottomNavigation extends StatefulWidget {
  const BottomNavigation({Key? key}) : super(key: key);

  @override
  _BottomNavigation createState() => _BottomNavigation();
}

class _BottomNavigation extends State<BottomNavigation>
    with TickerProviderStateMixin {
  final controller = Get.find<NavigationController>();
  List<String> listOfStrings = ['Beranda', 'Folder', 'Task', 'Calender'];
  List<IconData> listOfIcons = [
    Icons.home,
    Icons.folder,
    Icons.task,
    Icons.calendar_today
  ];

  @override
  Widget build(BuildContext context) {
    print(controller.currentIndex);
    double displayWidth = MediaQuery.of(context).size.width;
    return BottomNavyBar(
      items: [
        BottomNavyBarItem(
            icon: const Icon(Icons.home),
            title: const Text('Home'),
            activeColor: Colors.brown,
            inactiveColor: Colors.brown[200]),
        BottomNavyBarItem(
            icon: const Icon(Icons.folder),
            title: const Text('Users'),
            activeColor: Colors.brown,
            inactiveColor: Colors.brown[200]),
        BottomNavyBarItem(
            icon: const Icon(Icons.message),
            title: const Text('Messages'),
            activeColor: Colors.brown,
            inactiveColor: Colors.brown[200]),
        BottomNavyBarItem(
            icon: const Icon(Icons.settings),
            title: const Text('Settings'),
            activeColor: Colors.brown,
            inactiveColor: Colors.brown[200]),
      ],
      onItemSelected: (value) {
        setState(() {
          controller.onTabTapped(value);
        });
        controller.currentIndex == 2
            ? Navigator.push(
                context,
                PageTransition(
                    child: TaskScreen(), type: PageTransitionType.bottomToTop, duration: Duration(seconds: 1), reverseDuration: Duration(seconds: 1)))
            : null;
        controller.currentIndex == 1
            ? Navigator.push(
            context,
            PageTransition(
                child: TambahPage(), type: PageTransitionType.bottomToTop, duration: Duration(seconds: 1), reverseDuration: Duration(seconds: 1)))
            : null;
        controller.currentIndex == 0
            ? Navigator.push(
            context,
            PageTransition(
                child: HomeScreenPage(), type: PageTransitionType.bottomToTop, duration: Duration(seconds: 1), reverseDuration: Duration(seconds: 1)))
            : null;
        controller.currentIndex == 3
            ? Navigator.push(
            context,
            PageTransition(
                child: HealthScreen(), type: PageTransitionType.bottomToTop, duration: Duration(seconds: 1), reverseDuration: Duration(seconds: 1)))
            : null;
        // if (controller.currentIndex == 2) {
        //   Navigator.push(
        //       context,
        //       PageRouteBuilder(
        //         reverseTransitionDuration: Duration(seconds: 1),
        //         transitionDuration: const Duration(seconds: 1),
        //         pageBuilder: (context, animation, secondaryAnimation) {
        //           return TaskScreen();
        //         },
        //         transitionsBuilder:
        //             (context, animation, secondaryAnimation, child) {
        //           const begin = Offset(0.0, 1.0);
        //           const end = Offset.zero;
        //           const curve = Curves.ease;
        //
        //           var tween = Tween(begin: begin, end: end)
        //               .chain(CurveTween(curve: curve));
        //
        //           return SlideTransition(
        //             position: animation.drive(tween),
        //             child: child,
        //           );
        //         },
        //       ));
        // }
      },
      selectedIndex: controller.currentIndex,
    );
    //   Container(
    //   margin: EdgeInsets.all(displayWidth * .05),
    //   height: displayWidth * .155,
    //   decoration: BoxDecoration(
    //     color: Colors.white,
    //     boxShadow: [
    //       BoxShadow(
    //         color: Colors.black.withOpacity(.1),
    //         blurRadius: 30,
    //         offset: Offset(0, 10)
    //       )
    //     ],
    //     borderRadius: BorderRadius.circular(50)
    //   ),
    //   child: ListView.builder(
    //     scrollDirection: Axis.horizontal,
    //     itemCount: 4,
    //     itemBuilder: (context, index) {
    //       return InkWell(
    //         onTap: () {
    //           setState(() {
    //             controller.onTabTapped(index);
    //           });
    //         },
    //         splashColor: Colors.transparent,
    //         highlightColor: Colors.transparent,
    //         child: Stack(
    //           children: [
    //             AnimatedContainer(
    //               duration: Duration(seconds: 1),
    //               curve: Curves.fastLinearToSlowEaseIn,
    //               width: index == controller.currentIndex
    //                   ? displayWidth * .32
    //                   : displayWidth * .18,
    //               alignment: Alignment.center,
    //               child: AnimatedContainer(
    //                 duration: Duration(seconds: 1),
    //                 curve: Curves.fastLinearToSlowEaseIn,
    //                 height: index == controller.currentIndex
    //                     ? displayWidth * .12
    //                     : 0,
    //                 width: index == controller.currentIndex
    //                     ? displayWidth * .32
    //                     : 0,
    //                 decoration: BoxDecoration(
    //                     color: index == controller.currentIndex
    //                         ? Colors.brown[200]
    //                         : Colors.transparent,
    //                     borderRadius: BorderRadius.circular(50)),
    //               ),
    //             ),
    //             AnimatedContainer(
    //               duration: Duration(seconds: 1),
    //               curve: Curves.fastLinearToSlowEaseIn,
    //               width: index == controller.currentIndex
    //                   ? displayWidth * .32
    //                   : displayWidth * .18,
    //               alignment: Alignment.center,
    //               child: Stack(
    //                 children: [
    //                   Row(
    //                     children: [
    //                       AnimatedContainer(
    //                         duration: Duration(seconds: 1),
    //                         curve: Curves.fastLinearToSlowEaseIn,
    //                         width: index == controller.currentIndex
    //                             ? displayWidth * .13
    //                             : 0,
    //                       ),
    //                       AnimatedOpacity(
    //                         opacity: index == controller.currentIndex ? 1 : 0,
    //                         duration: Duration(seconds: 1),
    //                         curve: Curves.fastLinearToSlowEaseIn,
    //                         child: Text(
    //                           index == controller.currentIndex
    //                               ? '${listOfStrings[index]}'
    //                               : '',
    //                           style: TextStyle(
    //                               color: Colors.white,
    //                               fontWeight: FontWeight.w600,
    //                               fontSize: 15),
    //                         ),
    //                       )
    //                     ],
    //                   ),
    //                   Row(
    //                     children: [
    //                       AnimatedContainer(
    //                         duration: Duration(seconds: 1),
    //                         curve: Curves.fastLinearToSlowEaseIn,
    //                         width: index == controller.currentIndex
    //                             ? displayWidth * .03
    //                             : 20,
    //                       ),
    //                       Icon(
    //                         listOfIcons[index],
    //                         size: displayWidth * .076,
    //                         color: index == controller.currentIndex ? Colors.white : Colors.black26,
    //                       )
    //                     ],
    //                   )
    //                 ],
    //               ),
    //             )
    //           ],
    //         ),
    //       );
    //     },
    //   ),
    // );
    // return Container(
    //   height: 80,
    //   child: BottomNavigationBar(
    //     type: BottomNavigationBarType.fixed,
    //     currentIndex: controller.currentIndex,
    //     showSelectedLabels: false,
    //     showUnselectedLabels: false,
    //     selectedItemColor: Colors.red,
    //     unselectedItemColor: Colors.grey,
    //     onTap: controller.onTabTapped,
    //     items: const [
    //       BottomNavigationBarItem(
    //           icon: Icon(
    //             Icons.home,
    //             size: 25,
    //           ),
    //           label: 'Beranda'),
    //       BottomNavigationBarItem(
    //           icon: Icon(
    //             Icons.folder,
    //             size: 25,
    //           ),
    //           label: 'Tole'),
    //       BottomNavigationBarItem(
    //           icon: Icon(
    //             Icons.task,
    //             size: 25,
    //           ),
    //           label: 'Oke'),
    //       BottomNavigationBarItem(
    //           icon: Icon(
    //             Icons.calendar_today,
    //             size: 25,
    //           ),
    //           label: 'Sie')
    //     ],
    //   ),
    // );
  }
}
