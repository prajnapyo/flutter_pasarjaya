import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_learn/ui/home_screen.dart';

class StartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(60),
          child: AppBar(
            backgroundColor: const Color(0xffd1edbf),
            elevation: 0,
            leading: const Padding(
                padding: EdgeInsets.only(left: 20),
                child: Icon(
                  Icons.ac_unit,
                  color: Colors.black,
                  size: 30,
                )),
            title: const Text(
              'Dayzer',
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.w700),
            ),
            actions: [
              const Padding(
                padding: EdgeInsets.only(right: 20),
                child: Icon(Icons.star),
              )
            ],
          ),
        ),
        body: Container(
          padding: const EdgeInsets.all(20),
          color: const Color(0xffd1edbf),
          child: Center(
            child: Column(
              children: [
                const Text(
                  'To simplify',
                  style: TextStyle(fontWeight: FontWeight.w900, fontSize: 34),
                ),
                const Text('the way you',
                    style:
                        TextStyle(fontWeight: FontWeight.w900, fontSize: 34)),
                const Text('work',
                    style:
                        TextStyle(fontWeight: FontWeight.w900, fontSize: 34)),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.05,
                ),
                Image.asset('assets/images/r1.png'),
                const Spacer(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Controling deliveries in',
                        style: TextStyle(fontSize: 16)),
                    Text('Reliable and no-hassle way',
                        style: TextStyle(fontSize: 16)),
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.03,
                ),
                ElevatedButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (BuildContext context) {
                        return const HomeScreenPage();
                      }));
                    },
                    style:
                        ElevatedButton.styleFrom(backgroundColor: Colors.black, elevation: 10, shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))),
                    child: Container(
                      alignment: Alignment.center,
                      height: 60,
                      width: 300,
                      child: const Text(
                        'Get free trial',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                    ))
              ],
            ),
          ),
        ));
  }
}
