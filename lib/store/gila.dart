import 'package:flutter_learn/model/akun.dart';
import 'package:flutter_learn/service/akun_service.dart';
import 'package:mobx/mobx.dart';

part 'gila.g.dart';

class Gila = _Gila with _$Gila;

abstract class _Gila with Store {
  @observable
  ObservableList<Akun> akun = ObservableList<Akun>();
  String? name = '', alamat;
  int? age;

  AkunService service = AkunService();

  @observable
  ObservableFuture<List<Akun>> fetchTodosFuture = emptyResponse;

  @computed
  bool get hasResults =>
      fetchTodosFuture != emptyResponse &&
          fetchTodosFuture.status == FutureStatus.fulfilled;

  static ObservableFuture<List<Akun>> emptyResponse =
  ObservableFuture.value([]);

  @computed
  ObservableList<Akun> get dataAkun {
    return ObservableList.of(akun);
  }

  _Gila(){
    fetchAkun(name!);
  }

  @action
  Future<List<Akun>> fetchAkun(String name) async {
    akun.clear();
    final future = service.searchAkun(name);
    fetchTodosFuture = ObservableFuture(future);

    return akun = ObservableList.of(await future);
  }

  @action
  Future<int> deleteAkun(String id) async {
    final future = await service.deleteAkun(id);
    return future;
  }

  @action
  addAkun(String name, int age, String alamat) async {
    this.name = name;
    this.age = age;
    this.alamat = alamat;
  }
}
