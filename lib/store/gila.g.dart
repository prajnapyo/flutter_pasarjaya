// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gila.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$Gila on _Gila, Store {
  Computed<bool>? _$hasResultsComputed;

  @override
  bool get hasResults => (_$hasResultsComputed ??=
          Computed<bool>(() => super.hasResults, name: '_Gila.hasResults'))
      .value;
  Computed<ObservableList<Akun>>? _$dataAkunComputed;

  @override
  ObservableList<Akun> get dataAkun => (_$dataAkunComputed ??=
          Computed<ObservableList<Akun>>(() => super.dataAkun,
              name: '_Gila.dataAkun'))
      .value;

  late final _$akunAtom = Atom(name: '_Gila.akun', context: context);

  @override
  ObservableList<Akun> get akun {
    _$akunAtom.reportRead();
    return super.akun;
  }

  @override
  set akun(ObservableList<Akun> value) {
    _$akunAtom.reportWrite(value, super.akun, () {
      super.akun = value;
    });
  }

  late final _$fetchTodosFutureAtom =
      Atom(name: '_Gila.fetchTodosFuture', context: context);

  @override
  ObservableFuture<List<Akun>> get fetchTodosFuture {
    _$fetchTodosFutureAtom.reportRead();
    return super.fetchTodosFuture;
  }

  @override
  set fetchTodosFuture(ObservableFuture<List<Akun>> value) {
    _$fetchTodosFutureAtom.reportWrite(value, super.fetchTodosFuture, () {
      super.fetchTodosFuture = value;
    });
  }

  late final _$fetchAkunAsyncAction =
      AsyncAction('_Gila.fetchAkun', context: context);

  @override
  Future<List<Akun>> fetchAkun(String name) {
    return _$fetchAkunAsyncAction.run(() => super.fetchAkun(name));
  }

  late final _$deleteAkunAsyncAction =
      AsyncAction('_Gila.deleteAkun', context: context);

  @override
  Future<int> deleteAkun(String id) {
    return _$deleteAkunAsyncAction.run(() => super.deleteAkun(id));
  }

  late final _$addAkunAsyncAction =
      AsyncAction('_Gila.addAkun', context: context);

  @override
  Future addAkun(String name, int age, String alamat) {
    return _$addAkunAsyncAction.run(() => super.addAkun(name, age, alamat));
  }

  @override
  String toString() {
    return '''
akun: ${akun},
fetchTodosFuture: ${fetchTodosFuture},
hasResults: ${hasResults},
dataAkun: ${dataAkun}
    ''';
  }
}
