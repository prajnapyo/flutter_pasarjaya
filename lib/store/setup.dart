import 'package:flutter_learn/store/akun_store.dart';
import 'package:get_it/get_it.dart';

void setup() {
  GetIt.instance.registerSingleton<AkunStore>(AkunStore());
}
