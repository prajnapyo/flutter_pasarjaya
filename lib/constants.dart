import 'package:flutter/material.dart';

class ApiConstants {
  static String baseUrl = 'https://jsonplaceholder.typicode.com';
}

const connectTimeout = 30000;
const receiveTimeout = 30000;
const primaryColor = Color(0xffa26de6);
const primaryLightColor = Color(0xfff3f3f4);
const primaryGrayColor = Color(0xFFF2F2F2);
