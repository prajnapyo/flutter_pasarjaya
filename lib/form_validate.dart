import 'package:flashlight/flashlight.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_learn/home.dart';
import 'package:flutter_learn/service/api_service.dart';
import 'package:flutter_learn/service/secure_storage.dart';
import 'package:flutter_learn/store/akun_store.dart';
import 'package:flutter_learn/store/todo_store.dart';
import 'package:flutter_learn/ui/home.dart';
import 'package:flutter_learn/ui/home_screen.dart';
import 'package:flutter_learn/ui/navigation_step1.dart';
import 'package:flutter_learn/ui/start_screen.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';

class FormValidate extends StatefulWidget {
  const FormValidate({Key? key}) : super(key: key);

  @override
  _FormValidate createState() => _FormValidate();
}

class _FormValidate extends State<FormValidate> {
  bool _isObscure = true;
  final _formKey = GlobalKey<FormState>();
  var isLoading = false;
  int gender = 0;
  String? token;
  String? message;
  final TextEditingController username = TextEditingController();
  final TextEditingController password = TextEditingController();

  void _submit() {
    final isValid = _formKey.currentState?.validate();
    if (!isValid!) {
      return;
    }
    _formKey.currentState?.save();
  }

  final AkunStore store = AkunStore();

  final storage = const FlutterSecureStorage();
  final SecureStorage _secureStorage = SecureStorage();

  @override
  void initState() {
    super.initState();
    fetchSecureStorageData();
  }

  // Future<String?> getUserName() async {
  //   token = await storage.read(key: 'token');
  //   print(token);
  // }

  Future<void> fetchSecureStorageData() async {
    token = await _secureStorage.getToken() ?? '';
    message = await _secureStorage.getUserName() ?? '';
    // _passwordController.text = await _secureStorage.getPassWord() ?? '';
  }

  @override
  Widget build(BuildContext context) {
    print(token);
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Container(
                margin: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (BuildContext context) {
                          return StartScreen();
                        }));
                      },
                      icon: const Icon(
                        Icons.turn_left_rounded,
                        color: Colors.white,
                        size: 50,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    GestureDetector(
                      onTap: () {
                        PersistentNavBarNavigator.pushNewScreen(context,
                            screen: NavigationScreen());
                      },
                      child: const Text(
                        "Let's sign you in.",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                            fontSize: 36),
                      ),
                    ),
                    const Text(
                      "Welcome back.",
                      style: TextStyle(color: Colors.white, fontSize: 32),
                    ),
                    const Text(
                      "You've been missed!",
                      style: TextStyle(color: Colors.white, fontSize: 34),
                    )
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.all(25),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        controller: username,
                        keyboardType: TextInputType.emailAddress,
                        // validator: (value) {
                        //   if (value!.isEmpty ||
                        //       !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                        //           .hasMatch(value)) {
                        //     return 'Enter a valid Email!';
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        style: const TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.grey[700],
                            labelStyle: TextStyle(color: Colors.grey[500]),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15),
                                borderSide:
                                    const BorderSide(color: Colors.amber)),
                            labelText: 'username or email',
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15),
                                borderSide:
                                    const BorderSide(color: Colors.grey))),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                        controller: password,
                        obscureText: _isObscure,
                        // validator: (value) {
                        //   if (value!.isEmpty ||
                        //       !RegExp(r'^(?=.*?[A-Z])(?=.*[a-z])(?=.*?[0-9])')
                        //           .hasMatch(value)) {
                        //     return 'Enter a valid Password!';
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        style: const TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.grey[700],
                            labelStyle: TextStyle(color: Colors.grey[500]),
                            suffixIcon: IconButton(
                                onPressed: () {
                                  setState(() {
                                    _isObscure = !_isObscure;
                                  });
                                },
                                icon: Icon(
                                  _isObscure
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  color: Colors.grey,
                                )),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15)),
                            labelText: 'password',
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15),
                                borderSide:
                                    const BorderSide(color: Colors.grey))),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      CupertinoSlidingSegmentedControl(
                        backgroundColor: Colors.grey.shade700,
                        groupValue: gender,
                        children: <int, Widget>{
                          0: Padding(
                            padding: const EdgeInsets.fromLTRB(52, 15, 52, 15),
                            child: Text(
                              'Pria',
                              style: TextStyle(
                                  color:
                                      gender == 0 ? Colors.grey : Colors.black),
                            ),
                          ),
                          1: Padding(
                            padding: const EdgeInsets.fromLTRB(52, 15, 52, 15),
                            child: Text(
                              'Wanita',
                              style: TextStyle(
                                  color:
                                      gender == 1 ? Colors.grey : Colors.black),
                            ),
                          )
                        },
                        onValueChanged: (value) {
                          setState(() {
                            gender = int.tryParse(value.toString())!;
                          });
                        },
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height / 4,
                      ),
                      Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Don't have an account? ",
                                style: TextStyle(color: Colors.grey[500]),
                              ),
                              Text(
                                message ?? 'toel',
                                style: TextStyle(color: Colors.grey[500]),
                              ),
                              TextButton(
                                onPressed: () {
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (BuildContext context) {
                                    return const HomePage();
                                  }));
                                },
                                child: const Text(
                                  "Register",
                                  style: TextStyle(color: Colors.white),
                                ),
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          ElevatedButton(
                              onPressed: () async {
                                if (_formKey.currentState!.validate()) {
                                  await ApiService()
                                      .login(username.text, password.text)
                                      .then((value) {
                                    print(value);
                                  });
                                  // await ApiService().cobaApi(username.text, password.text).then((value) {
                                  //   print(value);
                                  //   String token = (value['token']);
                                  //   Map<String, dynamic> decodedToken = JwtDecoder.decode(token);
                                  //   print(decodedToken);
                                  //   DateTime expirationDate = JwtDecoder.getExpirationDate(token);
                                  //   print(expirationDate);
                                  //   showDialog(
                                  //       context: context,
                                  //       builder: (BuildContext context) =>
                                  //       const AlertDialog(
                                  //         title: Icon(
                                  //           Icons.check_circle,
                                  //           color: Colors.green,
                                  //           size: 32,
                                  //         ),
                                  //         content: Text(
                                  //           'Success Login',
                                  //           textAlign: TextAlign.center,
                                  //           style: TextStyle(
                                  //               fontWeight: FontWeight.w600),
                                  //         ),
                                  //       ));
                                  // }).catchError((e) {
                                  //   showDialog(
                                  //       context: context,
                                  //       builder: (BuildContext context) =>
                                  //       const AlertDialog(
                                  //         title: Icon(
                                  //           Icons.error,
                                  //           color: Colors.red,
                                  //         ),
                                  //         content: Text(
                                  //           'Failed Login',
                                  //           textAlign: TextAlign.center,
                                  //           style: TextStyle(
                                  //               fontWeight: FontWeight.w600),
                                  //         ),
                                  //       ));
                                  //   return throw Exception('salah cuy');
                                  // });
                                } else {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) =>
                                          const AlertDialog(
                                            title: Icon(
                                              Icons.error,
                                              color: Colors.red,
                                            ),
                                            content: Text(
                                              'Failed Login',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600),
                                            ),
                                          ));
                                }
                              },
                              style: ElevatedButton.styleFrom(
                                  foregroundColor: Colors.black,
                                  backgroundColor: Colors.white,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(25))),
                              child: Container(
                                alignment: Alignment.center,
                                height: 60,
                                width: 300,
                                child: const Text(
                                  "Sign In",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16),
                                ),
                              ))
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
