import 'package:flutter/material.dart';
import 'package:flutter_learn/model/akun.dart';
import 'package:flutter_learn/service/akun_service.dart';
import 'package:flutter_learn/store/akun_store.dart';
import 'package:flutter_learn/store/todo_store.dart';
import 'package:flutter_learn/ui/detail.dart';
import 'package:flutter_learn/ui/tambah.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mobx/mobx.dart';

class Home extends StatefulWidget {
  // const Home({Key? key, required this.store}) : super(key: key);
  // final AkunStore store;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  // _HomeState({Key? key, required this.store});
  int length = 10;
  String name = '';

  final AkunStore store = AkunStore();

  @override
  initState() {
    // this is called when the class is initialized or called for the first time
    super.initState();
    store;
    // _getData(); //  this is the material super constructor for init state to link your instance initState to the global initState context
  }

  void search(value) {
    setState(() {
      name = value;
    });
  }
  //
  // Future<List<Akun>> getData(String name) async {
  //   var values = (await AkunService().searchAkun(name));
  //   return values;
  // }
  //
  // Future<List<Akun>> _getData() async {
  //   var values = (await AkunService().getAkun());
  //   return values;
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Daftar Akun"),
        actions: [
          Padding(
            // ignore: prefer_const_constructors
            padding: EdgeInsets.all(10),
            child: ElevatedButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(
                    builder: (context) {
                      return const TambahPage();
                    },
                  ));
                },
                child: const Text('Tambah Akun')),
          )
        ],
      ),
      body: Container(
        margin: const EdgeInsets.all(10),
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(top: 10, bottom: 10),
              child: TextField(
                onChanged: (value) {
                  search(value);
                },
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  suffixIcon: const Icon(
                    Icons.search,
                    color: Colors.black,
                  ),
                  hintText: 'Search Name',
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20)),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20)),
                ),
              ),
            ),
            Observer(
              builder: (context) {
                if (store.dataAkun.isEmpty) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  return Expanded(
                    child: ListView.builder(
                        physics: const BouncingScrollPhysics(),
                        itemCount: store.dataAkun.length,
                        itemBuilder: (_, int index) {
                          final stores = store.dataAkun[index];
                          return Card(
                            child: Column(
                              children: [
                                ListTile(
                                  title: Text(stores.name ?? 'ss'),
                                  subtitle: Text(stores.alamat ?? ''),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      ElevatedButton.icon(
                                        style: ElevatedButton.styleFrom(
                                            backgroundColor:
                                                Colors.yellow.shade800),
                                        onPressed: () {
                                          Navigator.push(context,
                                              MaterialPageRoute(builder:
                                                  (BuildContext context) {
                                            return DetailPage(
                                              id: stores.id ?? '',
                                            );
                                          }));
                                        },
                                        icon: const Icon(Icons.book),
                                        label: const Text('Update'),
                                      ),
                                      const SizedBox(
                                        width: 20,
                                      ),
                                      ElevatedButton.icon(
                                        style: ElevatedButton.styleFrom(
                                            backgroundColor: Colors.red),
                                        onPressed: () {
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) =>
                                                  AlertDialog(
                                                    title: const Icon(
                                                      Icons.error,
                                                      color: Colors.red,
                                                    ),
                                                    content: const Text(
                                                        "Are you sure to delete this data ?"),
                                                    actions: [
                                                      TextButton(
                                                          onPressed: () async {
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                            await AkunService()
                                                                .deleteAkun(
                                                                    stores.id ??
                                                                        '')
                                                                .then(
                                                                    (value) async {
                                                              if (value == 1) {
                                                                setState(() {
                                                                  stores;
                                                                });
                                                                return Fluttertoast
                                                                    .showToast(
                                                                  msg:
                                                                      "Data Berhasil Dihapus",
                                                                );
                                                              } else {
                                                                return Fluttertoast
                                                                    .showToast(
                                                                  msg:
                                                                      "Data Gagal Dihapus",
                                                                );
                                                              }
                                                            });
                                                          },
                                                          child: const Text(
                                                              'Yes')),
                                                      TextButton(
                                                          onPressed: () {
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          },
                                                          child: const Text(
                                                              'Cancel'))
                                                    ],
                                                  ));
                                        },
                                        icon: const Icon(Icons.delete),
                                        label: const Text('Delete'),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          );
                        }),
                  );
                }
              },
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          store.fetchAkun(name);
        },
        child: const Icon(Icons.refresh),
      ),
    );
  }
}
