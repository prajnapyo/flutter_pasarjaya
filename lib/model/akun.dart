class Akun {
  String? id;
  String? name;
  int? age;
  String? alamat;
  String? createdAt;
  String? updatedAt;

  Akun(
      {this.id,
      this.name,
      this.age,
      this.alamat,
      this.createdAt,
      this.updatedAt});

  Akun.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    name = json["name"];
    age = json["age"];
    alamat = json["alamat"];
    createdAt = json["createdAt"];
    updatedAt = json["updatedAt"];
  }
}
