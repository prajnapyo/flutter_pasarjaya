import 'dart:convert';

class TypiCode {
  int? userId;
  int? id;
  String? title;
  String? body;

  TypiCode(
      { this.userId,
       this.id,
       this.title,
       this.body});

  TypiCode.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    userId = json["userId"];
    title = json["title"];
    body = json["body"];
  }
}
